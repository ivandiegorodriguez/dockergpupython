From computer running container

0 - create ssh-kegen and upload .pub to gitlab repos

1 - From terminal

bash script.sh

2 - logout to update docker with new user

3 - In machine running container

# set PATH for cuda installation
if [ -d "/usr/local/cuda/bin/" ]; then
    export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}
    export LD_LIBRARY_PATH=/usr/local/cuda/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
fi



Inside container


4 - Update path for GPU project  

export PATH="/usr/local/cuda-10.2/lib64:$PATH"
export PATH="~/Documents/GIT_PROJECTS/heapmemorypool/rmm:$PATH"
export PATH="~/Documents/GIT_PROJECTS/GPUAlgoDresden/gpu-collision-simulation/mallocMC:$PATH"
export PATH="~/Documents/GIT_PROJECTS/GPUAlgoDresden/gpu-collision-simulation/cmake:$PATH"

change openMesh branch
change gpuSimul branch to Dev one

5 - To run Jupyter notebooks:

a - connect Jupyter notebooks to docker : 

docker run -it -p 8888:8888 --gpus all -v ~/Projects/:/opt/IvanGitProjects  gpusimulation:latest /bin/bash

b - From local (ssh tunneling):

ssh -t -t ivan@gcp-IP -L 8888:localhost:8889

c - From Docker container: jupyter notebook --allow-root --no-browser --ip 0.0.0.0 --port 8888

d - Connect local browser to localhost:8888 and copy token from docker machine into local browser Jupyter screen
