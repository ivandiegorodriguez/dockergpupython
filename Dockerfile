FROM nvidia/cuda:10.2-cudnn7-devel-ubuntu18.04

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /opt

# Build tools
RUN apt-get update && \
    apt-get install -y \
    sudo \
    tzdata \
    git \
    cmake \
    wget \
    unzip \
    build-essential\
    wget\
    tar
    

# Python:
RUN sudo apt-get install -y \
    build-essential\
    python3-pip\
    python3-dev \
    python3-tk \
    python3-numpy\
    nano\
    libxt-dev\
    libgl1-mesa-glx




RUN pip3 install jupyter


# CMAKE 3.18.4 required for openMP and cuda/C++ compilation
RUN sudo apt-get install libssl-dev


# CMAKE
RUN sudo apt remove -y --purge --auto-remove cmake
    
ADD https://github.com/Kitware/CMake/releases/download/v3.18.4/cmake-3.18.4.tar.gz /opt
RUN tar -xzvf /opt/cmake-3.18.4.tar.gz
RUN cd /opt/cmake-3.18.4 && \
    ./bootstrap && \
     make -j8 && \
    sudo make install





# Download boost, untar, setup install with bootstrap and only do the Program Options library,
# and then install
RUN cd /home && wget http://downloads.sourceforge.net/project/boost/boost/1.67.0/boost_1_67_0.tar.gz \
  && tar xfz boost_1_67_0.tar.gz \
  && rm boost_1_67_0.tar.gz \
  && cd boost_1_67_0 \
  && ./bootstrap.sh --prefix=/usr/local --with-libraries=program_options \
  && ./b2 install \
  && cd /home \
  && rm -rf boost_1_67_0

# Clone git repository with dummy C++ program, use make to compile, install it, then remove the repo
RUN cd /home \
  && git clone https://github.com/pblischak/boost-docker-test.git \
  && cd /home/boost-docker-test \
  && make linux \
  && make install \
  && cd .. \
  && rm -rf boost-docker-test









# INSTALL ANACONDA AND CREATE ENVIRONMENT: Install libraries needed for 3D gpu simulation: VTK, OCC, etc.

# curl is required to download Anaconda.
RUN apt-get update && apt-get install curl -y

# Download and install Anaconda.
RUN mkdir /opt/tmp
RUN cd /opt/tmp && curl -O https://repo.anaconda.com/archive/Anaconda3-2019.07-Linux-x86_64.sh
RUN chmod +x /opt/tmp/Anaconda3-2019.07-Linux-x86_64.sh
RUN mkdir /root/.conda
RUN bash -c "/opt/tmp/Anaconda3-2019.07-Linux-x86_64.sh -b -p ${CONDA_PATH}"

RUN cd /opt/tmp && git clone https://gitlab.com/ivandiegorodriguez/dockergpupython.git

# Initializes Conda for bash shell interaction.
ENV PATH="/root/anaconda3/bin:$PATH"

RUN conda init bash

# Upgrade Conda to the latest version
RUN conda update -n base -c defaults conda -y

# Create the work environment and setup its activation on start.
RUN conda create --name gpuSimulation -y
RUN echo conda activate gpuSimulation >> /root/.bashrc
#RUN conda activate gpuSimulation >> /root/.bashrc
#COPY environment.yml .
#RUN conda create -f /opt/tmp/dockergpupython/environment.yml -n bla1
RUN conda env update -n gpuSimulation --file /opt/tmp/dockergpupython/environment.yml




###### gitlab ssh key access

ARG SSH_PRIVATE_KEY
RUN mkdir /root/.ssh/
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
#COPY ../id_rsa  /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
# Use git with SSH instead of https
RUN echo "[url \"git@gitlab.com:\"]\n\tinsteadOf = https://gitlab.com/" >> /root/.gitconfig
RUN echo "StrictHostKeyChecking no " > /root/.ssh/config






#### TO DO ---> MISSING BOOST LIBRARY 
#### TO DO ---> MISSING CUPLA PATH IN BASHRC
#### TO DO ---> MISSING MALLOC_MC PATH IN BASHRC
#### TO DO ---> MISSING CMAKE PATH IN BASHRC (it include FindMallocMC.cmake)
#### TO DO ---> change OPENMESH branch to Voxel2Mesh (NOT HERE REALLY)
#### TO DO ---> DOCKER IMAGES IN CUSTOM FOLDER



# Clone git project

#RUN pip3 -q install pip --upgrade

#RUN pip3 install jupyter

#RUN sudo apt-get install -y libboost-all-dev



#CMD open bash

