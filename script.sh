#!/bin/bash

#### Install docker ####
sudo apt update
sudo apt -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
apt-cache policy docker-ce
sudo apt -y install docker-ce
sudo groupadd docker
sudo gpasswd -a $USER docker
sudo service docker start

#### Change docker folder to store images

sudo service docker stop
sudo cp daemon.json /etc/docker
sudo rsync -aP /var/lib/docker/ /ivandev/docker
sudo mv /var/lib/docker /var/lib/docker.old
sudo service docker start
sudo rm -rf /var/lib/docker.old
#docker build --tag exportvoxelenv:1.0 .
sudo docker build --tag gpusimulation --build-arg SSH_PRIVATE_KEY="$(cat ~/.ssh/id_rsa)" .

#### Install vscode ####
#sudo apt update
#sudo apt install software-properties-common apt-transport-https wget
#wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
#sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
#sudo apt update
#sudo apt install code

#### Install NVIDIA drivers

sudo apt-get update
sudo apt-get upgrade

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600 
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub 
sudo add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /" 
sudo apt-get update 

sudo apt-get -y install cuda


##### Install NVIDA docker toolkit

distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker
